using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Banana : MonoBehaviour
{
    private Rigidbody rb;
    public ScriptableItems attributos;
    private GamePool pool;
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        pool = transform.parent.GetComponent<GamePool>();
    }

    private void OnEnable()
    {
        Debug.Log("Inicia");
        //transform.position = Vector3.Slerp(transform.position, transform.forward * attributos.impulse,1f);
        rb.AddForce(transform.forward * attributos.impulse+transform.up * attributos.impulse, ForceMode.Impulse);
        //StartCoroutine(Desactiva());
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<PlayerProva>(out PlayerProva pp))
        {
            pool.ReturnElement(gameObject);
            if (!pp.invincible)
            {
                //TODO -> In Player
                if(pp.TryGetComponent<Rigidbody>(out Rigidbody rb))
                {
                    rb.velocity = Vector3.zero;
                };
            }
        }
    }
}
