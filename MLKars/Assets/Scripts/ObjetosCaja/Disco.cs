using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disco : MonoBehaviour
{
    private Rigidbody rb;
    public ScriptableItems attributos;
    [SerializeField] private int max_Rebotes;
    private GamePool pool;
    private int n_Rebotes;
    private bool no_Colisiona = false;

    public float impulse;
    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        pool = transform.parent.GetComponent<GamePool>();
    }
    private void OnEnable()
    {
        transform.GetComponent<MeshCollider>().isTrigger = true;
        Debug.Log("Inicia");
        rb.AddForce(transform.forward * (impulse + attributos.impulse),ForceMode.VelocityChange);
        StartCoroutine(Desactiva());
    }

    IEnumerator Desactiva()
    {
        int i = 0;
        no_Colisiona = true;
        while (i < attributos.time)
        {
            if (i == 1)
            {
                no_Colisiona = false;
                transform.GetComponent<MeshCollider>().isTrigger = false;
            }
            i++;
            yield return new WaitForSeconds(1);
        }
        pool.ReturnElement(gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (!no_Colisiona)
        {
            if (collision.gameObject.TryGetComponent<PlayerProva>(out PlayerProva pp))
            {
                //TODO -> In Player
                pool.ReturnElement(gameObject);
                if (!pp.invincible)
                {
                pp.gameObject.SetActive(false);
                }
            }
            else
            {
                n_Rebotes++;
                compruebaRebotes(n_Rebotes);
            }
        }
    }
    private void compruebaRebotes(int rebotes)
    {
        if(rebotes >= max_Rebotes)
        {
            pool.ReturnElement(gameObject);
        }
    }
}
