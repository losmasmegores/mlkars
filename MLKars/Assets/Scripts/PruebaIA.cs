﻿using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class PruebaIA : MonoBehaviour
{
    [SerializeField] private Transform[] checkpoints;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private LayerMask layers;
    [SerializeField] private int actualCheckpoint;
    private void Awake()
    {
        actualCheckpoint = 0;
        agent.SetDestination(checkpoints[actualCheckpoint].position);
    }
    
    private void Update()
    {
        // if (Physics.CheckSphere(transform.position, 1f, layers))
        // {
        //     if(actualCheckpoint == checkpoints.Length - 1)
        //         actualCheckpoint = 0;
        //     else
        //         actualCheckpoint++;
        //     agent.SetDestination(checkpoints[actualCheckpoint].position);
        // }
        //Esta opcion tambien es valida manito
        // if (Vector3.Distance(transform.position, checkpoints[actualCheckpoint].position) < 0.5f)
        // {
        //     if(actualCheckpoint == checkpoints.Length - 1)
        //         actualCheckpoint = 0;
        //     else
        //         actualCheckpoint++;
        //     actualCheckpoint++;
        //     agent.SetDestination(checkpoints[actualCheckpoint].position);
        // }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.position == checkpoints[actualCheckpoint].transform.position)
        {
            if(actualCheckpoint == checkpoints.Length - 1)
                actualCheckpoint = 0;
            else
                actualCheckpoint++;
            agent.SetDestination(checkpoints[actualCheckpoint].position);
        }
        
    }
    //draw gizmos
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 1f);
    }
}