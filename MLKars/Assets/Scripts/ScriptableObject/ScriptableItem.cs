using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableItems", menuName = "Scriptable Objects/Scriptable Items")]
[Serializable]
public class ScriptableItems: ScriptableObject
{
    [SerializeField] private TipoItem tipo;
    [SerializeField] private int usos;
    [SerializeField] private float fImpulso;
    [SerializeField] private float duracion;
    [SerializeField] private TipoLanzable comoLanza;

    public TipoItem type => tipo;
    public int nUsos
    {
        get { return usos; }   // get method
        set { usos = value; }  // set method
    }
    public float impulse => fImpulso;
    public float time => duracion;
    public TipoLanzable lanzable => comoLanza;
}
