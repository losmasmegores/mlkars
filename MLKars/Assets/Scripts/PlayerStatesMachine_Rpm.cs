
using Grpc.Core.Logging;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.VFX;
using Input = UnityEngine.Windows.Input;

public class PlayerStatesMachine_Rpm : MonoBehaviour
{
    /*private enum MachineStates { IDLE, ACELERANDO, FRENANDO, DERRAPANDO, SALTAR, MUELTO};
    private MachineStates m_CurrentState;*/

    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    private Rigidbody m_Rigidbody;

    [SerializeField] private TrailRenderer[] trails;
    [SerializeField] private VisualEffect humoizq;
    [SerializeField] private VisualEffect humoder;


    [Header("Character Values")]
    [SerializeField] WheelCollider frontRight;
    [SerializeField] WheelCollider frontLeft;
    [SerializeField] WheelCollider backRight;
    [SerializeField] WheelCollider backLeft;

    [SerializeField] Transform frontRightT;
    [SerializeField] Transform frontLeftT;
    [SerializeField] Transform backRightT;
    [SerializeField] Transform backLeftT;

    public float acceleration = 500f;
    public float breakingForce = 300f;
    public float maxTurnAngle = 20f;

    private float currentAcceleration = 0f;
    private float currentBreakingForce = 0f;
    private float currentTurnAngle = 0f;

    WheelFrictionCurve wfc;
    [SerializeField] private float m_extremumSlip;
    [SerializeField] private float m_extremumValue;
    [SerializeField] private float m_asymptoteSlip;
    [SerializeField] private float m_asymptoteValue;
    [SerializeField] private float m_stiffness;


    /*private void ChangeState(MachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        Debug.Log("Changed State to " + newState);
        InitState(newState);
    }*/

    /*private void InitState(MachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case MachineStates.IDLE:
                break;

            case MachineStates.ACELERANDO:

                break;
            case MachineStates.FRENANDO:
                break;
            case MachineStates.DERRAPANDO:
                
                break;
            case MachineStates.SALTAR:
                break;
            default:
                break;
        }
    }*/

    /*private void ExitState()
    {
        switch (m_CurrentState)
        {
            case MachineStates.IDLE:

                break;

            case MachineStates.ACELERANDO:

                break;
            case MachineStates.FRENANDO:
                break;
            case MachineStates.DERRAPANDO:
                
                break;
            case MachineStates.SALTAR:
                break;
            default:
                break;
        }
    }*/

    /*private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case MachineStates.IDLE:
                if (m_MovementAction.ReadValue<Vector2>().sqrMagnitude > 0)
                {
                    ChangeState(MachineStates.ACELERANDO);
                }
                frontRight.brakeTorque = currentBreakingForce;
                frontLeft.brakeTorque = currentBreakingForce;
                backRight.brakeTorque = currentBreakingForce;
                backLeft.brakeTorque = currentBreakingForce;

                break;
            default:
                break;
        }
    }*/



    void Awake()
    {
        Assert.IsNotNull(m_InputAsset);

        m_Input = Instantiate(m_InputAsset);
        m_MovementAction = m_Input.FindActionMap("Character").FindAction("Movement");
        m_Input.FindActionMap("Character").FindAction("Brake").performed += Derrapar;
        m_Input.FindActionMap("Character").FindAction("Brake").canceled += OutDerrapar;
        m_Input.FindActionMap("Character").FindAction("TurnUp").canceled += PonerBien;
        m_Input.FindActionMap("Character").Enable();

        m_Rigidbody = GetComponent<Rigidbody>();



    }

    private void PonerBien(InputAction.CallbackContext obj)
    {
        transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
    }
    
    private void Start()
    {
        //InitState(MachineStates.IDLE);
    }

    void FixedUpdate()
    {
        // Debug.Log(backLeftT.position);
        // Debug.Log(humoizq.GetVector3("Posicion"));
        // Aplica aceleración a las ruedas de delante
        frontRight.motorTorque = currentAcceleration;
        frontLeft.motorTorque = currentAcceleration;

        // Aplica ángulo de giro a las ruedas delanteras
        frontLeft.steerAngle = currentTurnAngle;
        frontRight.steerAngle = currentTurnAngle;

        // Aplica fuerza de frenado a las ruedas de atrás
        backRight.brakeTorque = currentBreakingForce;
        backLeft.brakeTorque = currentBreakingForce;
        frontLeft.brakeTorque = currentBreakingForce;
        frontRight.brakeTorque = currentBreakingForce;

        // Actualiza la posición de las ruedas
        ActualizaWheel(frontRight, frontRightT);
        ActualizaWheel(frontLeft, frontLeftT);
        ActualizaWheel(backRight, backRightT);
        ActualizaWheel(backLeft, backLeftT);
    }

    private void Update()
    {
        humoizq.SetVector3("Posicion", backLeftT.position);
        humoder.SetVector3("Posicion", backRightT.position);
        currentAcceleration = m_MovementAction.ReadValue<Vector2>().y * acceleration;
        if (m_MovementAction.ReadValue<Vector2>().y < 0)
        {
            currentAcceleration = currentAcceleration * 2;
        }
        currentTurnAngle = m_MovementAction.ReadValue<Vector2>().x * maxTurnAngle;
        
    }

    private void OnDestroy()
    {
        m_Input.FindActionMap("Character").Disable();
        m_Input.FindAction("Brake").Disable();
    }

    Vector3 pos;
    Quaternion rot;
    void ActualizaWheel(WheelCollider col, Transform t)
    {
        col.GetWorldPose(out pos, out rot);
        t.position = pos;
        t.rotation = rot;
    }

    void Derrapar(InputAction.CallbackContext ctx)
    {
        currentBreakingForce = breakingForce;
        wfc.extremumSlip = m_extremumSlip;
        wfc.extremumValue = m_extremumSlip;
        wfc.asymptoteSlip = m_asymptoteSlip;
        wfc.asymptoteValue = m_asymptoteSlip;
        wfc.stiffness = m_stiffness;
        backRight.sidewaysFriction = wfc;
        backLeft.sidewaysFriction = wfc;
    
        foreach (TrailRenderer t in trails)
        {
            t.emitting = true;
        }

        humoizq.Play();
        humoder.Play();
    }

    void OutDerrapar(InputAction.CallbackContext ctx)
    {
        currentBreakingForce = 0f;
        wfc.extremumSlip = 0.2f;
        wfc.extremumValue = 1;
        wfc.asymptoteSlip = 0.5f;
        wfc.asymptoteValue = 0.75f;
        wfc.stiffness = 1;
        backRight.sidewaysFriction = wfc;
        backLeft.sidewaysFriction = wfc;
        
        foreach (TrailRenderer t in trails)
        {
            t.emitting = false;
        }
        humoizq.Stop();
        humoder.Stop();
    }



}


