using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Vueltas : MonoBehaviour
{
    public GameEvent GameEventMeta;
    public GameEvent GameEventCheck;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("He colisionado con: "+ other.transform.name);
        Debug.Log("Tiene el tag: "+ other.transform.tag);
        if (other.transform.tag.Equals("Meta"))
        {
            GameEventMeta.Raise();
            
        }
        if (other.transform.tag.Equals("Chek"))
        {
            GameEventCheck.Raise();
        }
    }
}
