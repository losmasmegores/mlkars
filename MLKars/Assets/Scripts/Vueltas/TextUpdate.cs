using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TextUpdate : MonoBehaviour
{
    private TextMeshProUGUI  txt;
    private int contador = 0;
    public bool check = false;
    void Start()
    {
        txt = GetComponent<TextMeshProUGUI>();
        txt.text = "Vuelta 0";
    }


    public void CambioText()
    {
        
        if (check)
        {            
            contador++;
            txt.text = "Vuelta " + contador;
            check = false;
        }

        if (contador == 2)
        {
            SceneManager.LoadScene("EndGame");
        }
    }

    public void Check()
    {
        check = true;
    }
}
