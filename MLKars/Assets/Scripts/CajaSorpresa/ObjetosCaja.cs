using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetosCaja : MonoBehaviour
{
    [SerializeField] private ScriptableItems[] objetos;

    public ScriptableItems[] items => objetos;
}
