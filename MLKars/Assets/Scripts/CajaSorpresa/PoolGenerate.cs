using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolGenerate : MonoBehaviour
{
    private GamePool Pool;
    void Awake()
    {
        Pool = GetComponent<GamePool>();
    }

    public void ReGenerate()
    {
        Debug.Log("Regeneramos");
        StartCoroutine(Generate());
    }

    IEnumerator Generate()
    {
        int i = 0;
        while (i < 10)
        {
            i++;
            yield return new WaitForSeconds(1);
        }
        GameObject caja = Pool.GetElement();
        caja.SetActive(true);
    }
}
