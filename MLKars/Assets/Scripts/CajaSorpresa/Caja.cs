using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caja : MonoBehaviour
{
    private ScriptableItems item;
    [SerializeField] private ObjetosCaja objetos;
    [SerializeField] GamePool myPool;

    public ScriptableItems objeto => item;
    public delegate void regenerate();
    public event regenerate generaCaja;
    void Awake()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (generaCaja != null)
        {
            generaCaja.Invoke();
        }
        myPool.ReturnElement(this.gameObject);
    }
    private void OnEnable()
    {
        item = objetos.items[Random.Range(0, objetos.items.Length)];
        generaCaja += transform.parent.GetComponent<PoolGenerate>().ReGenerate;
    }
    private void OnDisable()
    {
        generaCaja -= transform.parent.GetComponent<PoolGenerate>().ReGenerate;
    }
    //Make the cube rotate in x, y and z
    void FixedUpdate()
    {
        transform.Rotate(15 * Time.deltaTime, 30 * Time.deltaTime, 45 * Time.deltaTime);
    }
}
