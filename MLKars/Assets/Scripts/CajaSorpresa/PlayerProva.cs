using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEditor.Experimental.GraphView.GraphView;

public class PlayerProva : MonoBehaviour
{
    
    private Rigidbody rb;
    private bool invencible;
    [SerializeField] private GamePool Discos;
    [SerializeField] private GamePool Platanos;
    [SerializeField] private float colRadius;
    [SerializeField] private float impulso;
    [SerializeField] private LayerMask layers;
    public bool invincible => invencible;
    [SerializeField] private ScriptableItems item;
    [SerializeField] private int usos;
    [SerializeField] private Transform itempoint;
    [SerializeField] private Shader invencibleShader;
    [SerializeField] private ParticleSystem particulas;
    [SerializeField] private GameEvent InvencibleEvent;
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        if (item != null) usos = item.nUsos;
    }

    void Update()
    {
        //TODO -> PASAR EL INPUT A INPUT SYSTEM
        if (Input.GetKeyDown(KeyCode.LeftShift))
            UsarItem();
        // if (Physics.CheckSphere(transform.position, colRadius, layers))
        // {
        //     Debug.Log("gotta go fast");
        //     rb.AddForce(transform.forward * impulso, ForceMode.Impulse);
        // }
    }
    
    public void UsarItem()
    {
        if (item != null)
        {
            if (item.type == TipoItem.Impulso)
            {
                Debug.Log("Fast");
                rb.AddForce(transform.forward * item.impulse, ForceMode.Impulse);
            }
            else if (item.type == TipoItem.Mejora)
            {
                invencible = true;
                StartCoroutine(soyinvencible());
            }
            else
            {
                GameObject o;
                if (item.lanzable == TipoLanzable.Rebota)
                {
                    o = Discos.GetElement();
                    //TODO -> funcion dar atributos();
                    o.GetComponent<Disco>().attributos = item;
                    o.GetComponent<Disco>().impulse = rb.velocity.magnitude;
                }
                else
                {
                    o = Platanos.GetElement();
                    o.GetComponent<Banana>().attributos = item;
                    // o.transform.position = itempoint.forward * item.impulse + itempoint.up * item.impulse ; //
                }
                o.transform.position = itempoint.position;
                o.transform.forward = itempoint.forward;
                //o.transform.position = new Vector3(transform.position.x, transform.position.y+1, transform.position.z + 1);
                // o.transform.Rotate(0, itempoint.eulerAngles.y, 0);
                o.SetActive(true);
            }
            if (usos < 1) item = null;
            else usos -= 1;
        }
    }
    IEnumerator soyinvencible()
    {
        float time = item.time;
        InvencibleEvent.Raise();
        int i = 0;
        while(i < time)
        {
            Debug.Log("Soy Invencible");
            i++;
            
            yield return new WaitForSeconds(1);
        }
        invencible = false;
        InvencibleEvent.Raise();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<Caja>(out Caja c))
        {
            this.item = c.objeto;
            Debug.Log(item);
            this.usos = c.objeto.nUsos;
        };
        if (other.gameObject.layer == 10)
        {
            Debug.Log("gotta go fast");
            rb.AddForce(transform.forward * impulso, ForceMode.Acceleration);
            particulas.Play();
        }
    }
    // private void OnDrawGizmosSelected()
    // {
    //     Gizmos.color = new Color(1, 0, 0, 0.4f);
    //     Gizmos.DrawSphere(transform.position, colRadius);
    // }
}
