using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvencibleEvent : MonoBehaviour
{
    private Material original;
    [SerializeField] private Material invencible;
    private Renderer rend;
    void Awake()
    {
        rend = GetComponent<Renderer>();
        original = rend.material;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void Invencible()
    {
        if (GetComponent<Renderer>().material == original)
        {
            rend.SetMaterials(new List<Material>(1){invencible});
            // float lerp = Mathf.PingPong(Time.time, 1) / 1;
            // rend.material.Lerp(original, invencible, lerp);   
        }
        else
        {
            rend.SetMaterials(new List<Material>(1){original});
            // float lerp = Mathf.PingPong(Time.time, 1) / 1;
            // rend.material.Lerp(invencible, original, lerp);   
        }
    }
}
