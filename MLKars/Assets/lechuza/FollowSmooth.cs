using System;
using UnityEngine;
using UnityEngine.UI;

public class FollowSmooth : MonoBehaviour
{
    public Transform pivote;
    public Transform pivoteLejano;
    public float speed = 10F;
    public Transform coche;
    private GravityMOD gMod;

    private void Start()
    {
        gMod = GameObject.FindWithTag("Coche").GetComponent<GravityMOD>();
        this.transform.position = pivote.transform.position;
    }

    private void FixedUpdate()
    {
        if (gMod.GetTocandoSuelo())
        {
            Vector3 cpos = pivote.position;
            Vector3 spos = Vector3.Lerp(transform.position, cpos, speed * Time.deltaTime);
            transform.position = spos;
            Vector3 forward = coche.position - spos;
            transform.forward = Vector3.Lerp(transform.forward, forward, Time.deltaTime);
            transform.LookAt(coche);
        }
        else
        {
            Vector3 cpos = pivoteLejano.position;
            Vector3 spos = Vector3.Lerp(transform.position, cpos, Time.deltaTime);
            transform.position = spos;
            Vector3 forward = coche.position - spos;
            transform.forward = Vector3.Lerp(transform.forward, forward, Time.deltaTime);
            transform.LookAt(coche);
        }
    }


}

