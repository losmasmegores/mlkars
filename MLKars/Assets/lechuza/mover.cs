using UnityEngine;

namespace m17
{
    public class mover : MonoBehaviour
    {
        [SerializeField]
        private float m_Speed;

        [SerializeField]
        private float m_RotationSpeed = 180f;
        //private float m_MouseSensitivity = 1f;
        Rigidbody m_Rigidbody;

        private void Awake()
        {
            m_Rigidbody = GetComponent<Rigidbody>();
        }

        void Update()
        {
            //rotate
            if (Input.GetKey(KeyCode.A))
                transform.Rotate(-Vector3.up * m_RotationSpeed * Time.deltaTime);
            if (Input.GetKey(KeyCode.D))
                transform.Rotate(Vector3.up * m_RotationSpeed * Time.deltaTime);

            Vector3 movement = Vector3.zero;

            if (Input.GetKey(KeyCode.W))
            {
                movement += transform.forward;
            }
            if (Input.GetKey(KeyCode.S))
            {
                movement -= transform.forward;
            }


            movement.Normalize();
            movement *= m_Speed;
            m_Rigidbody.velocity = movement;


        }
    }
}
