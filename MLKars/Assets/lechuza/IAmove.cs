
using System.Numerics;
using JetBrains.Annotations;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using Vector3 = UnityEngine.Vector3;

public class IAmove : MonoBehaviour
{
    [SerializeField] private Transform[] checkpoints;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private LayerMask layers;
    [CanBeNull] public Transform curren;
    private int actualCheckpoint;
    private void Awake()
    {
        actualCheckpoint = 0;
        agent.SetDestination(checkpoints[actualCheckpoint].position);
    }
    
    private void Update()
    {
        // if (Physics.CheckSphere(transform.position, 1f, layers))
        // {
        //     actualCheckpoint++;
        //     agent.SetDestination(checkpoints[actualCheckpoint].position);
        // }
        //Esta opcion tambien es valida manito
        Debug.Log(Vector3.Distance(transform.position, checkpoints[actualCheckpoint].position));
        if (Vector3.Distance(transform.position, checkpoints[actualCheckpoint].position) < 0.6f)
        {
            actualCheckpoint++;
            agent.SetDestination(checkpoints[actualCheckpoint].position);
            curren = checkpoints[actualCheckpoint];
        }
    }
}
