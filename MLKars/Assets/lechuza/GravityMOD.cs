using UnityEngine;
using UnityEngine.SceneManagement;

public class GravityMOD : MonoBehaviour
{
    private Rigidbody rb;
    public float Gravity;
    private bool tocandoSuelo = true;
    void Awake()
    {
        rb = this.GetComponent<Rigidbody>();
    }
    void FixedUpdate()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position+transform.up, -transform.up, out hit, 2f))
        {
            Debug.DrawRay(transform.position+transform.up, transform.up * -2f, Color.red, 1f);
            rb.AddForce( this.transform.up*-Gravity, ForceMode.Acceleration);
            tocandoSuelo = true;
        }
        else
        {
            tocandoSuelo = false;
        }

    }

    public bool GetTocandoSuelo()
    {
        return tocandoSuelo;
    }
}

