using UnityEngine;
using UnityEngine.InputSystem;

public class WheelControl : MonoBehaviour
{
    [SerializeField] WheelCollider frontRight;
    [SerializeField] WheelCollider frontLeft;
    [SerializeField] WheelCollider backRight;
    [SerializeField] WheelCollider backLeft;

    [SerializeField] Transform frontRightT;
    [SerializeField] Transform frontLeftT;
    [SerializeField] Transform backRightT;
    [SerializeField] Transform backLeftT;

    public float acceleration = 500f;
    public float breakingForce = 300f;
    public float maxTurnAngle = 20f;

    private float currentAcceleration = 0f;
    private float currentBreakingForce = 0f;
    private float currentTurnAngle = 0f;

    private void FixedUpdate()
    {
        //currentAcceleration = Input.GetAxis("Vertical") * acceleration;
        //currentTurnAngle = Input.GetAxis("Horizontal") * maxTurnAngle;
        /*currentAcceleration = m_MovementAction.ReadValue<Vector2>().y * acceleration;
        currentTurnAngle = m_MovementAction.ReadValue<Vector2>().x * maxTurnAngle;*/

        // Aplica aceleración a las ruedas de delante
        frontRight.motorTorque = currentAcceleration;
        frontLeft.motorTorque = currentAcceleration;

        // Aplica fuerza de frenado a todas las ruedas
        currentBreakingForce = Input.GetKey(KeyCode.Space) ? breakingForce : 0f;
        frontRight.brakeTorque = currentBreakingForce;
        frontLeft.brakeTorque = currentBreakingForce;
        backRight.brakeTorque = currentBreakingForce;
        backLeft.brakeTorque = currentBreakingForce;

        // Aplica ángulo de giro a las ruedas delanteras
        frontLeft.steerAngle = currentTurnAngle;
        frontRight.steerAngle = currentTurnAngle;

        // Actualiza la posición de las ruedas
        ActualizaWheel(frontRight, frontRightT);
        ActualizaWheel(frontLeft, frontLeftT);
        ActualizaWheel(backRight, backRightT);
        ActualizaWheel(backLeft, backLeftT);
    }

    void ActualizaWheel(WheelCollider col, Transform t)
    {
        Vector3 pos;
        Quaternion rot;
        col.GetWorldPose(out pos, out rot);
        t.position = pos;
        t.rotation = rot;
    }

}
